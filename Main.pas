unit Main;

interface

uses
  Vcl.Dialogs, Vcl.ExtDlgs, Vcl.Controls, Vcl.StdCtrls, System.Classes, Vcl.Forms, System.SysUtils, IniFiles, FileCtrl,
  Math, ShellAPI, Messages, IOUtils, Vcl.ExtCtrls;

type

  TFormMain = class(TForm)
    OpenPicDialogAtlas: TOpenPictureDialog;
    EditNameAtlas: TEdit;
    BtnBrowseAtlas: TButton;
    BtnCutAtlas: TButton;
    GrpBoxCut: TGroupBox;
    GrpBoxCreate: TGroupBox;
    BtnBrowseDir: TButton;
    BtnCreateAtlas: TButton;
    EditDirFiles: TEdit;
    LabelPathAtlas: TLabel;
    LabelPathDir: TLabel;
    LabelStatusCut: TLabel;
    LabelStatusCreate: TLabel;
    ComboBoxWidthAtlas: TComboBox;
    LabelWidthAtlas: TLabel;
    ComboBoxSelectLang: TComboBox;
    LabelSelectLang: TLabel;
    OpenDialog1: TOpenDialog;
    LabelAtlasType: TLabel;
    ComboBoxAtlasType: TComboBox;
    CheckBoxTrimAtlas: TCheckBox;
    ComboBoxHieghtAtlas: TComboBox;
    LabelHieghtAtlas: TLabel;
    procedure BtnBrowseAtlasClick(Sender: TObject);
    procedure BtnCutAtlasClick(Sender: TObject);
    procedure BtnBrowseDirClick(Sender: TObject);
    procedure BtnCreateAtlasClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ComboBoxSelectLangSelect(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure WMDropFiles(var Msg: TWMDropFiles); message WM_DROPFILES;
    procedure ComboBoxAtlasTypeSelect(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormMain: TFormMain;

implementation

uses CreationAtlas, CuttingAtlas, Language, Constants;

{$R *.dfm}

procedure SetCompCaption;
var
  Index: Integer;
begin
  with FormMain do
  begin
    GrpBoxCreate.Caption := IniLanguage.GrpBoxCreateCaption;
    BtnBrowseDir.Caption := IniLanguage.BtnBrowseDirCaption;
    BtnCreateAtlas.Caption := IniLanguage.BtnCreateAtlasCaption;
    LabelPathDir.Caption := IniLanguage.LabelPathDirCaption;
    LabelWidthAtlas.Caption := IniLanguage.LabelWidthAtlasCaption;
    LabelHieghtAtlas.Caption := IniLanguage.LabelHeightAtlasCaption;
    GrpBoxCut.Caption := IniLanguage.GrpBoxCutCaption;
    BtnBrowseAtlas.Caption := IniLanguage.BtnBrowseAtlasCaption;
    BtnCutAtlas.Caption := IniLanguage.BtnCutAtlasCaption;
    LabelPathAtlas.Caption := IniLanguage.LabelPathAtlasCaption;
    LabelSelectLang.Caption := IniLanguage.LabelSelectLangCaption;
    LabelAtlasType.Caption := IniLanguage.LabelAtlasTypeCaption;
    CheckBoxTrimAtlas.Caption := IniLanguage.CheckBoxTrimAtlasCaption;
    Index := ComboBoxHieghtAtlas.ItemIndex;
    ComboBoxHieghtAtlas.Items[4] := IniLanguage.Auto;
    ComboBoxHieghtAtlas.ItemIndex := Index;
    Index := ComboBoxAtlasType.ItemIndex;
    ComboBoxAtlasType.Items.Text := IniLanguage.ComboBoxAtlasTypeItems;
    ComboBoxAtlasType.ItemIndex := Index;
    ComboBoxAtlasType.Hint := IniLanguage.ComboBoxAtlasTypeHint;
  end;
end;

procedure TFormMain.WMDropFiles(var Msg: TWMDropFiles);
var
  DropH: HDROP;
  DroppedFileCount: Integer;
  FileNameLength: Integer;
  FileName: string;
begin
  inherited;
  DropH := Msg.Drop;
  try
    DroppedFileCount := DragQueryFile(DropH, $FFFFFFFF, nil, 0);
    if DroppedFileCount > 0 then
    begin
      FileNameLength := DragQueryFile(DropH, 0, nil, 0);
      SetLength(FileName, FileNameLength);
      DragQueryFile(DropH, 0, PChar(FileName), FileNameLength + 1);
      if TFileAttribute.faDirectory in TPath.GetAttributes(FileName, True) then
        EditDirFiles.Text := FileName
      else
        EditNameAtlas.Text := FileName;
    end;
  finally
    DragFinish(DropH);
  end;
  Msg.Result := 0;
end;

procedure TFormMain.BtnBrowseDirClick(Sender: TObject);
var
  dir: string;
begin
  if System.SysUtils.DirectoryExists(EditDirFiles.Text) then
    dir := EditDirFiles.Text
  else if System.SysUtils.DirectoryExists(ExtractFilePath(EditNameAtlas.Text)) then
    dir := ExtractFilePath(EditNameAtlas.Text)
  else
    dir := ExtractFilePath(ParamStr(0));
  if SelectDirectory(IniLanguage.LabelSelectDir, '', dir, [sdShowShares, sdNewUI]) then
  begin
    EditDirFiles.Text := dir;
    LabelStatusCreate.Caption := '';
  end;
end;

procedure TFormMain.BtnBrowseAtlasClick(Sender: TObject);
begin
  OpenPicDialogAtlas.Filter := IniLanguage.OpenDialogAtlasFilter;
  if OpenPicDialogAtlas.Execute() then
  begin
    EditNameAtlas.Text := OpenPicDialogAtlas.FileName;
    LabelStatusCut.Caption := '';
  end;
end;

procedure TFormMain.BtnCutAtlasClick(Sender: TObject);
var
  NumberError: TIdErorr;
begin
  NumberError := CutAtlas(EditNameAtlas.Text);
  if NumberError = erNotEerror then
    LabelStatusCut.Caption := IniLanguage.ImagesExtract // '����������� ��������� �� ������.'
  else
    LabelStatusCut.Caption := IniLanguage.ImagesNotExtract; // '�� ������� ������� ����������� �� ������.';

  case NumberError of
    erUnsupportedFormat:
      ShowMessage(IniLanguage.InvalidImage);
    erFilesNotFound, erXmlNotFound, erImageNotFound:
      ShowMessage(IniLanguage.NotFindAtlas); // '���� ��� ��� ����� ������ �� ������(-�)';
  end;
end;

procedure TFormMain.ComboBoxAtlasTypeSelect(Sender: TObject);
begin
  if TAtlasType(ComboBoxAtlasType.ItemIndex) = atDXT5w then
    LabelStatusCreate.Caption := IniLanguage.Warning
  else
    LabelStatusCreate.Caption := '';
end;

procedure TFormMain.ComboBoxSelectLangSelect(Sender: TObject);
begin
  IniLanguage.LoadFromFile(ExtractFilePath(ParamStr(0)) + '\' + ComboBoxSelectLang.Items[ComboBoxSelectLang.ItemIndex]
      + '.lng');
  SetCompCaption;
end;

procedure TFormMain.FormClose(Sender: TObject; var Action: TCloseAction);
var
  IniSetting: TIniFile;
  LastLangauge: string;
begin
  IniSetting := TIniFile.Create(ExtractFilePath(ParamStr(0)) + '\Setting.ini');
  LastLangauge := ComboBoxSelectLang.Text;
  IniSetting.WriteString('Setting', 'LastLangauge', LastLangauge);
  IniSetting.WriteInteger('Setting', 'AtlasType', ComboBoxAtlasType.ItemIndex);
  IniSetting.WriteBool('Setting', 'TrimAtlas', CheckBoxTrimAtlas.Checked);
  IniSetting.WriteInteger('Setting', 'AtlasWidth', ComboBoxWidthAtlas.ItemIndex);
  IniSetting.WriteInteger('Setting', 'AtlasHieght', ComboBoxHieghtAtlas.ItemIndex);
  DragAcceptFiles(Handle, False);
end;

procedure FillComboBox;
var
  SR: TSearchRec;
begin
  if FindFirst(ExtractFilePath(ParamStr(0)) + '\*.lng', faAnyFile, SR) = 0 then
  begin
    repeat
      if (SR.Attr <> faDirectory) then
        FormMain.ComboBoxSelectLang.Items.Append(ChangeFileExt(SR.Name, ''));
    until FindNext(SR) <> 0;
    FormMain.ComboBoxSelectLang.ItemIndex := 0;
    FindClose(SR);
  end;
end;

procedure TFormMain.FormCreate(Sender: TObject);
var
  SR: TSearchRec;
  IniSetting: TIniFile;
  LastLangauge: string;
begin
//  ShowMessage(IntToStr(Trunc(log2(1)) + 1));
  DragAcceptFiles(Handle, True);
  FillComboBox();
  LastLangauge := '';
  if FindFirst(ExtractFilePath(ParamStr(0)) + '\Setting.ini', faAnyFile, SR) = 0 then
    if (SR.Attr <> faDirectory) then
    begin
      IniSetting := TIniFile.Create(ExtractFilePath(ParamStr(0)) + '\Setting.ini');
      LastLangauge := IniSetting.ReadString('Setting', 'LastLangauge', '');
      ComboBoxAtlasType.ItemIndex := IniSetting.ReadInteger('Setting', 'AtlasType', 0);
      CheckBoxTrimAtlas.Checked := IniSetting.ReadBool('Setting', 'TrimAtlas', False);
      ComboBoxWidthAtlas.ItemIndex := IniSetting.ReadInteger('Setting', 'AtlasWidth', 2);
      ComboBoxHieghtAtlas.ItemIndex := IniSetting.ReadInteger('Setting', 'AtlasHieght', 2);
      ComboBoxAtlasTypeSelect(ComboBoxAtlasType);
    end;
  if ComboBoxSelectLang.ItemIndex >= 0 then
  begin
    if FindFirst(ExtractFilePath(ParamStr(0)) + '\Setting.ini', faAnyFile, SR) = 0 then
      if (SR.Attr <> faDirectory) then
      begin
        IniSetting := TIniFile.Create(ExtractFilePath(ParamStr(0)) + '\Setting.ini');
        LastLangauge := IniSetting.ReadString('Setting', 'LastLangauge', '');
      end;
    if LastLangauge <> '' then
      ComboBoxSelectLang.ItemIndex := Max(0, ComboBoxSelectLang.Items.IndexOf(LastLangauge));
    IniLanguage.LoadFromFile(ExtractFilePath(ParamStr(0)) + '\' + ComboBoxSelectLang.Items[ComboBoxSelectLang.ItemIndex]
        + '.lng');
  end;
  SetCompCaption;

end;

procedure TFormMain.BtnCreateAtlasClick(Sender: TObject);
const
  AHeight: array [0 .. 5] of Integer = (8192, 4096, 2048, 1024, 512, 0);
  AWidht: array [0 .. 4] of Integer = (8192, 4096, 2048, 1024, 512);
var
  NumberError: TIdErorr;
  // t, t1: Integer;
begin
  // t:= GetTickCount;
  if Trim(EditDirFiles.Text) <> '' then
    with EditDirFiles, CheckBoxTrimAtlas do
    begin
      NumberError := CreateAtlas(Trim(Text), '', AWidht[ComboBoxWidthAtlas.ItemIndex], AHeight[ComboBoxHieghtAtlas.ItemIndex],
        TAtlasType(ComboBoxAtlasType.ItemIndex), Checked);
      if NumberError = erNotEerror then
        LabelStatusCreate.Caption := IniLanguage.CreateAtlas // '����� ������.'
        // LabelStatusCreate.Caption:= FloatToStr((t1) / 1000) + 'c' + ', ' + FloatToStr((GetTickCount - t) / 1000) + 'c'
        // LabelStatusCreate.Caption:= FloatToStr(t1)
      else
        LabelStatusCreate.Caption := IniLanguage.NotCreateAtlas; // '����� �� ������.'
      case NumberError of
        erSmallSizeAtlas:
          ShowMessage(IniLanguage.ImpossiblePlace); // '��������� �� ����� ���������� ��������� ����� � ������.'
        erImagesPngNotFound:
          ShowMessage(IniLanguage.NotFindImages); // '� ��������� �������� �� ������� ����� ������� PNG.'
      end;
    end
  else
    ShowMessage(IniLanguage.NotPathCatalog); // '�� ������ ���� � �������� � �������.'
end;

end.
