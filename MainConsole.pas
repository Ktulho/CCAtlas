unit MainConsole;

interface

uses
  SysUtils, Generics.Collections, IOUtils, Zip, Windows;

procedure RunConsole();

implementation

uses
  CreationAtlas, CuttingAtlas, Constants; // , stopwatch;

type
  TOption = record
    Key: UnicodeString;
    Value: TList<UnicodeString>;
  end;

var
  Parameters: TList<TOption>;

procedure DisplayErrorMessage(const ErrorCode: TIdErorr; const FirstParam: string = ''; const SecondParam: string = '');
begin
  case ErrorCode of    // erNotEerror:  ;
    erUnsupportedFormat:
      Writeln(Format('Not supported file format "%s".', [FirstParam]));
    erFilesNotFound:
      Writeln('The files are not found.');
    erXmlNotFound:
      Writeln(Format('The "%s" file was not found.', [ChangeFileExt(FirstParam, '.xml')]));
    erImageNotFound:
      Writeln(Format('The "%s" file was not found.', [FirstParam]));
    erPathNotSpecified:
      Writeln('The path is not specified.');
    erImagesPngNotFound:
      Writeln('PNG files were not found in the selected directory.');
    erAtlasNotCreate:
      Writeln('The Atlas could not be created.');
    erSmallSizeAtlas:
      Writeln('The program cannot place the selected files in the Atlas. Try increasing the size of the Atlas.');
    erHeightIncorrectly:
      Writeln('The height parameter is set incorrectly.');
    erWidthIncorrectly:
      Writeln('The width parameter is set incorrectly.');
    erAtlasTypeIncorrectly:
      Writeln('The Atlas type parameter is set incorrectly.');
    erFolderNotFound:
      Writeln(Format('Folder "%s" not found.', [FirstParam]));
    // erNotFoundKey: ;
    erNotWotOrDist:
      Writeln('You must specify the game directory and the image directory.');
    erNotFindZip:
      Writeln('The "gui.pkg" file was not found. Make sure that the path to the game directory is correct.');
    erMissingKey:
      Writeln(Format('Missing key. The "%s" parameter will be skipped.', [FirstParam]));
    erKeyNotExist:
      Writeln(Format('The "%s" key does not exist. The following settings will not be applied: %s', [FirstParam, SecondParam]));
  end;

end;

procedure SetParameters();
var
  I: integer;
  Option: TOption;
begin
  I := 1;
  while I <= ParamCount do
    if (ParamStr(I).IndexOfAny(['-', '/']) = 0) then
    begin
      Option.Key := ParamStr(I).Substring(1).ToLower;
      Inc(I);
      Option.Value := TList<UnicodeString>.Create();
      while (I <= ParamCount) and (ParamStr(I).IndexOfAny(['-', '/']) <> 0) do
      begin
        Option.Value.Add(ParamStr(I));
        Inc(I);
      end;
      Parameters.Add(Option);
    end
    else
    begin
      DisplayErrorMessage(erMissingKey, ParamStr(I));
      Inc(I)
    end;
end;

function GetParametr(Key: string): TList<string>;
var
  I: integer;
begin
  I := 0;
  while (I < Parameters.Count) and (Key <> Parameters[I].Key) do
    Inc(I);
  if I < Parameters.Count then
    Result := Parameters[I].Value
  else
    Result := nil;
end;

procedure CutAtlas(const Paths: TList<string>);
var
  ErrorCode: TIdErorr;
  OutPath: string;
begin
  OutPath := EmptyStr;
  if Paths.Count > 0 then
  begin
    if (Paths.Count > 1) and (TPath.HasValidPathChars(Paths[1], False)) then
      OutPath := IncludeTrailingPathDelimiter(Paths[1])
    else
      OutPath := ExtractFilePath(Paths[0]) + TPath.GetFileNameWithoutExtension(Paths[0]) + '\';
    ErrorCode := CuttingAtlas.CutAtlas(Paths[0], OutPath);
  end
  else
    ErrorCode := erPathNotSpecified;

  case ErrorCode of
    erNotEerror:
      Writeln(Format('Files created in the folder "%s".', [OutPath]));
  else
    DisplayErrorMessage(ErrorCode, Paths[0]);
  end;
end;

function ExtractFile(const PathWoT: string; const AtlasName: string): TIdErorr;
var
  Zip: TZipFile;
  PackageName, TempFolder, FileName: string;
  I: integer;
begin
  Result := erImageNotFound;
  Zip := TZipFile.Create();
  try
    TempFolder := TPath.GetTempPath();
    try
      I := 1;
      PackageName := TPath.Combine(PathWoT, 'res\packages\gui-part1.pkg');
      while FileExists(PackageName) do
      begin
        Zip.Open(PackageName, zmRead);
        for FileName in Zip.FileNames do
          if FileName = AtlasName then
          begin
            Zip.Extract(FileName, TempFolder, False);
            Result := erNotEerror;
            break;
          end;
        Zip.Close;
        if Result = erNotEerror then
          break;
        Inc(I);
        PackageName := TPath.Combine(PathWoT, Format('res\packages\gui-part%u.pkg', [I]));
      end;
    except
      on E: EZipException do
        Result := erNotFindZip
    end;
  finally
    FreeAndNil(Zip);
  end;
end;

procedure CutAtlasFromZip(const Paths: TList<string>; const AtlasName: string);
var
  ErrorCode: TIdErorr;
  TempFolder: string;
begin
  if Paths.Count > 1 then
  try
    TempFolder := TPath.GetTempPath();
    ErrorCode := ExtractFile(Paths[0], Format('gui/flash/atlases/%s.dds', [AtlasName]));
    if ErrorCode = erNotEerror then
      ErrorCode := ExtractFile(Paths[0], Format('gui/flash/atlases/%s.xml', [AtlasName]));
    if ErrorCode = erNotEerror then
      ErrorCode := CuttingAtlas.CutAtlas(TPath.Combine(TempFolder, Format('%s.dds', [AtlasName])), IncludeTrailingPathDelimiter(Paths[1]));
  finally
    SysUtils.DeleteFile(TPath.Combine(TempFolder, Format('%s.dds', [AtlasName])));
    SysUtils.DeleteFile(TPath.Combine(TempFolder, Format('%s.xml', [AtlasName])));
  end
  else
    ErrorCode := erNotWotOrDist;

  case ErrorCode of
    erNotEerror:
      Writeln(Format('Files created in the folder "%s".', [Paths[1]]));
  else
    DisplayErrorMessage(ErrorCode, Format('%s.dds', [AtlasName]));
  end;
end;

procedure SetHeightAtlas(var Height: integer; var ErrorCode: TIdErorr);
var
  Options: TList<string>;
begin
  Options := GetParametr(HEIGHT_ATLAS);
  if (Options <> nil) and (Options.Count > 0) then
  try
    Height := Options[0].ToInteger();
    Inc(Height, (4 - (Height and 3)) and 3);
      // �� �� �����: if (Height mod 4) > 0 then Inc(Height, 4 - (Height mod 4));
  except
    on E: EConvertError do
      ErrorCode := erHeightIncorrectly;
  end;
end;

procedure SetWidthAtlas(var Width: integer; var ErrorCode: TIdErorr);
var
  Options: TList<string>;
begin
  Options := GetParametr(WIDTH_ATLAS);
  if (Options <> nil) and (Options.Count > 0) then
  try
    Width := Options[0].ToInteger();
    Inc(Width, (4 - (Width and 3)) and 3);
  except
    on E: EConvertError do
      ErrorCode := erWidthIncorrectly;
  end;
end;

procedure SetTypeAtlas(var AtlasType: TAtlasType; var ErrorCode: TIdErorr);
var
  Options: TList<string>;
begin
  Options := GetParametr(ATLAS_TYPE);
  if (Options <> nil) and (Options.Count > 0) then
    if Options[0].ToLower() = 'png' then
      AtlasType := atPNG
    else if Options[0].ToLower() = 'dds' then
      AtlasType := atDXT5
    else if Options[0].ToLower() = 'dds_w' then
      AtlasType := atDXT5w
    else if Options[0].ToLower() = 'dds_w_womip' then
      AtlasType := atDXT5w_withoutMIP
    else
      ErrorCode := erAtlasTypeIncorrectly;
end;

procedure CreateAtlas(const Paths: TList<string>);
var
  ErrorCode: TIdErorr;
  OutPath: string;
  Width, Height: integer;
  AtlasType: TAtlasType;
  isTrimAtlas: Boolean;
begin
  ErrorCode := erNotEerror;
  Width := 4096;
  Height := 0;
  AtlasType := atPNG;
  SetHeightAtlas(Height, ErrorCode);
  SetWidthAtlas(Width, ErrorCode);
  SetTypeAtlas(AtlasType, ErrorCode);

  isTrimAtlas := GetParametr(TRIM_ATLAS) <> nil;

  if ErrorCode = erNotEerror then
    if Paths.Count > 0 then
      if DirectoryExists(Paths[0]) then
      begin
        if (Paths.Count > 1) and (TPath.HasValidPathChars(Paths[1], False)) then
          OutPath := Paths[1]
        else
          OutPath := IncludeTrailingPathDelimiter(Paths[0]) + 'Atlas\' + ExtractFileName(ExcludeTrailingPathDelimiter(Paths[0])) + ATLAS_EXTENSION[ord(AtlasType)];
        ErrorCode := CreationAtlas.CreateAtlas(Paths[0], OutPath, Width, Height, AtlasType, isTrimAtlas);
      end
      else
        ErrorCode := erFolderNotFound
    else
      ErrorCode := erPathNotSpecified;

  case ErrorCode of
    erNotEerror:
      Writeln(Format('Atlas "%s" created.', [OutPath]));
  else
    DisplayErrorMessage(ErrorCode, Paths[0]);
  end;
end;

procedure ConversionAtlas(const ID: integer; const Value: TList<UnicodeString>);
begin
  case ID of
    ID_CUT_ATLAS:
      CutAtlas(Value);
    ID_ASSEMBLE_ATLAS:
      CreateAtlas(Value);
    ID_CUT_BATTLE_ATLAS:
      CutAtlasFromZip(Value, 'battleAtlas');
    ID_CUT_MARKER_ATLAS:
      CutAtlasFromZip(Value, 'vehicleMarkerAtlas');
    ID_CUT_DAMAGE_INDICATOR:
      CutAtlasFromZip(Value, 'damageIndicator');
    ID_CUT_BATTLE_LOBBY:
      CutAtlasFromZip(Value, 'commonBattleLobby');
    ID_CUT_COMPONENTS_ATLAS:
      CutAtlasFromZip(Value, 'components');
    ID_CUT_MAPS_BLACKLIST:
      CutAtlasFromZip(Value, 'mapsBlacklist');
    ID_CUT_QUESTS_PROGRESS:
      CutAtlasFromZip(Value, 'questsProgress');
    ID_CUT_STORE_ATLAS:
      CutAtlasFromZip(Value, 'store');
  end
end;

procedure RunConsole();
var
  I: integer;
  Option: TOption;
  IndexByName: TDictionary<string, Byte>;
  // t: Cardinal;
begin
  // t := GetTickCount;
  Parameters := TList<TOption>.Create();
  // Writeln(CmdLine);
  try
    SetParameters();
    IndexByName := TDictionary<string, Byte>.Create(Length(ALL_PARAMETERS));
    try
      for I := 0 to NUMBER_PARAMETERS - 1 do
        IndexByName.Add(ALL_PARAMETERS[I], I);

      for Option in Parameters do
        if IndexByName.ContainsKey(Option.Key) then
          ConversionAtlas(IndexByName.Items[Option.Key], Option.Value)
        else
          DisplayErrorMessage(erKeyNotExist, Option.Key, string.Join(', ', Option.Value.ToArray));
    finally
      FreeAndNil(IndexByName);
    end;

    // for Option in Parameters do
    // begin
    // write('Key = ' + Option.Key);
    // for I := 0 to Option.Value.Count - 1 do
    // write(#$9 + 'Value[' + IntToStr(I) + '] = ' + Option.Value[I]);
    // Writeln;
    // end;
    // writeln(Cardinal(GetTickCount - t).ToString);

    // readln;
  finally
    FreeAndNil(Parameters);
  end;

end;

end.

