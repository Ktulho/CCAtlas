unit language;

interface

uses
  SysUtils, IniFiles;

type
  TIniLanguage = class(TObject)
  private
  public
    ImpossiblePlace: string;
    NotFindImages: string;
    NotFindAtlas: string;
    ImagesExtract: string;
    ImagesNotExtract: string;
    CreateAtlas: string;
    NotCreateAtlas: string;
    NotPathCatalog: string;
    FormCaption: string;
    GrpBoxCreateCaption: string;
    BtnBrowseDirCaption: string;
    BtnCreateAtlasCaption: string;
    LabelPathDirCaption: string;
    LabelWidthAtlasCaption: string;
    LabelHeightAtlasCaption: string;
    GrpBoxCutCaption: string;
    BtnBrowseAtlasCaption: string;
    BtnCutAtlasCaption: string;
    LabelPathAtlasCaption: string;
    LabelSelectLangCaption: string;
    LabelSelectDir: string;
    OpenDialogAtlasFilter: string;
    LabelAtlasTypeCaption: string;
    InvalidImage: string;
    ComboBoxAtlasTypeItems: string;
    CheckBoxTrimAtlasCaption: string;
    Warning: string;
    Auto: string;
    ComboBoxAtlasTypeHint: string;
    procedure LoadSettings(Ini: TIniFile);
    procedure SaveSettings(Ini: TIniFile);
    procedure LoadFromFile(const FileName: string);
    procedure SaveToFile(const FileName: string);
    constructor Create;
  end;

var
  IniLanguage: TIniLanguage = nil;

implementation

const
  Translation = 'Translation';

procedure TIniLanguage.LoadSettings(Ini: TIniFile);
begin
  if Ini <> nil then
  begin
    ImpossiblePlace := Ini.ReadString(Translation, 'ImpossiblePlace', ImpossiblePlace);
    NotFindImages := Ini.ReadString(Translation, 'NotFindImages', NotFindImages);
    NotFindAtlas := Ini.ReadString(Translation, 'NotFindAtlas', NotFindAtlas);
    ImagesExtract := Ini.ReadString(Translation, 'ImagesExtract', ImagesExtract);
    ImagesNotExtract := Ini.ReadString(Translation, 'ImagesNotExtract', ImagesNotExtract);
    CreateAtlas := Ini.ReadString(Translation, 'CreateAtlas', CreateAtlas);
    NotCreateAtlas := Ini.ReadString(Translation, 'NotCreateAtlas', NotCreateAtlas);
    NotPathCatalog := Ini.ReadString(Translation, 'NotPathCatalog', NotPathCatalog);
    FormCaption := Ini.ReadString(Translation, 'FormCaption', FormCaption);
    GrpBoxCreateCaption := Ini.ReadString(Translation, 'GrpBoxCreateCaption', GrpBoxCreateCaption);
    BtnBrowseDirCaption := Ini.ReadString(Translation, 'BtnBrowseDirCaption', BtnBrowseDirCaption);
    BtnCreateAtlasCaption := Ini.ReadString(Translation, 'BtnCreateAtlasCaption', BtnCreateAtlasCaption);
    LabelPathDirCaption := Ini.ReadString(Translation, 'LabelPathDirCaption', LabelPathDirCaption);
    LabelWidthAtlasCaption := Ini.ReadString(Translation, 'LabelWidthAtlasCaption', LabelWidthAtlasCaption);
    LabelHeightAtlasCaption := Ini.ReadString(Translation, 'LabelHeightAtlasCaption', LabelHeightAtlasCaption);
    GrpBoxCutCaption := Ini.ReadString(Translation, 'GrpBoxCutCaption', GrpBoxCutCaption);
    BtnBrowseAtlasCaption := Ini.ReadString(Translation, 'BtnBrowseAtlasCaption', BtnBrowseAtlasCaption);
    BtnCutAtlasCaption := Ini.ReadString(Translation, 'BtnCutAtlasCaption', BtnCutAtlasCaption);
    LabelPathAtlasCaption := Ini.ReadString(Translation, 'LabelPathAtlasCaption', LabelPathAtlasCaption);
    LabelSelectLangCaption := Ini.ReadString(Translation, 'LabelSelectLangCaption', LabelSelectLangCaption);
    LabelSelectDir := Ini.ReadString(Translation, 'LabelSelectDir', LabelSelectDir);
    OpenDialogAtlasFilter := Ini.ReadString(Translation, 'OpenDialogAtlasFilter', OpenDialogAtlasFilter);
    LabelAtlasTypeCaption := Ini.ReadString(Translation, 'LabelAtlasTypeCaption', LabelAtlasTypeCaption);
    InvalidImage := Ini.ReadString(Translation, 'InvalidImage', InvalidImage);
    CheckBoxTrimAtlasCaption := Ini.ReadString(Translation, 'CheckBoxTrimAtlasCaption', CheckBoxTrimAtlasCaption);
    Auto := Ini.ReadString(Translation, 'Auto', Auto);
    Warning := StringReplace(Ini.ReadString(Translation, 'Warning', Warning), '%n', sLineBreak, [rfReplaceAll]);
    ComboBoxAtlasTypeItems := StringReplace(Ini.ReadString(Translation, 'ComboBoxAtlasTypeItems', ComboBoxAtlasTypeItems), '%n', sLineBreak, [rfReplaceAll]);
    ComboBoxAtlasTypeHint := StringReplace(Ini.ReadString(Translation, 'ComboBoxAtlasTypeHint', ComboBoxAtlasTypeHint), '%n', sLineBreak, [rfReplaceAll]);
  end;
end;

procedure TIniLanguage.SaveSettings(Ini: TIniFile);
begin
  if Ini <> nil then
  begin
  end;
end;

constructor TIniLanguage.Create;
begin
  ImpossiblePlace := '��������� �� ����� ���������� ��������� ����� � ������.';
  NotFindImages := '� ��������� �������� �� ������� ����� ������� PNG.';
  NotFindAtlas := '���� ��� ��� ����� ������ �� ������(-�).';
  ImagesExtract := '����������� ��������� �� ������.';
  ImagesNotExtract := '�� ������� ������� ����������� �� ������.';
  CreateAtlas := '����� ������.';
  NotCreateAtlas := '����� �� ������.';
  NotPathCatalog := '�� ������ ���� � �������� � �������.';
  FormCaption := 'CCAtlas';
  GrpBoxCreateCaption := '�������� ������ �� ������';
  BtnBrowseDirCaption := '�����...';
  BtnCreateAtlasCaption := '�������';
  LabelPathDirCaption := '����';
  LabelWidthAtlasCaption := '������ ������';
  LabelHeightAtlasCaption := '������ ������';
  GrpBoxCutCaption := '���������� ������ �� �����';
  BtnBrowseAtlasCaption := '�����...';
  BtnCutAtlasCaption := '���������';
  LabelPathAtlasCaption := '����';
  LabelSelectLangCaption := '���� ����������';
  LabelSelectDir := '�������� ����� � �������������';
  OpenDialogAtlasFilter :=
    '��� ���� ����������� (*.png, *.dds)|*.png; *.dds|Portable Network Graphics (*.png)|*.png|DirectDraw Surface (*.dds)|*.dds|��� (*.*)|*.*';
  LabelAtlasTypeCaption := '��� ������';
  InvalidImage := '�� �������������� ����� �����������.';
  CheckBoxTrimAtlasCaption := '�������� ��������� �����';
  Auto := '����';
  Warning := '��������!!! ������� ������ ������ ��������������� ���������.';
  ComboBoxAtlasTypeItems := 'PNG WoT' + #13#10
                          + 'DDS WoT' + #13#10
                          + 'DDS WoWs' + #13#10
                          + 'DDS WoWs ��� MIP';
  ComboBoxAtlasTypeHint := 'PNG WoT - ����� � ������� PNG ��� World of Tanks' + #13#10
                         + 'DDS WoT - ����� � ������� DDS ��� World of Tanks' + #13#10
                         + 'DDS WoW - ����� � ������� DDS ��� World of Warships' + #13#10
                         + 'DDS WoW ��� MIP - ����� � ������� DDS ��� World of Warships, MIP maps �� ���������';
end;

procedure TIniLanguage.LoadFromFile(const FileName: string);
var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(FileName);
  try
    LoadSettings(Ini);
  finally
    FreeAndNil(Ini);
  end;
end;

procedure TIniLanguage.SaveToFile(const FileName: string);
var
  Ini: TIniFile;
begin
  Ini := TIniFile.Create(FileName);
  try
    SaveSettings(Ini);
  finally
    FreeAndNil(Ini);
  end;
end;

initialization

IniLanguage := TIniLanguage.Create();

finalization

FreeAndNil(IniLanguage);

end.
