object FormMain: TFormMain
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = #1057#1056#1040#1090#1083#1072#1089
  ClientHeight = 286
  ClientWidth = 417
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object LabelSelectLang: TLabel
    Left = 239
    Top = 261
    Width = 90
    Height = 13
    Alignment = taRightJustify
    Caption = #1071#1079#1099#1082' '#1080#1085#1090#1077#1088#1092#1077#1081#1089#1072
  end
  object GrpBoxCut: TGroupBox
    Left = 8
    Top = 8
    Width = 401
    Height = 87
    Caption = #1056#1072#1079#1076#1077#1083#1077#1085#1080#1077' '#1072#1090#1083#1072#1089#1072' '#1085#1072' '#1092#1072#1081#1083#1099
    TabOrder = 0
    object LabelPathAtlas: TLabel
      Left = 9
      Top = 21
      Width = 25
      Height = 13
      Caption = #1055#1091#1090#1100
    end
    object LabelStatusCut: TLabel
      Left = 9
      Top = 55
      Width = 3
      Height = 13
    end
    object BtnBrowseAtlas: TButton
      Left = 315
      Top = 17
      Width = 76
      Height = 23
      Caption = #1054#1073#1079#1086#1088'...'
      TabOrder = 0
      OnClick = BtnBrowseAtlasClick
    end
    object BtnCutAtlas: TButton
      Left = 315
      Top = 51
      Width = 76
      Height = 23
      Caption = #1056#1072#1079#1088#1077#1079#1072#1090#1100
      TabOrder = 1
      OnClick = BtnCutAtlasClick
    end
    object EditNameAtlas: TEdit
      Left = 40
      Top = 18
      Width = 265
      Height = 21
      TabOrder = 2
    end
  end
  object GrpBoxCreate: TGroupBox
    Left = 8
    Top = 103
    Width = 401
    Height = 146
    Caption = #1057#1086#1079#1076#1072#1085#1080#1077' '#1072#1090#1083#1072#1089#1072' '#1080#1079' '#1092#1072#1081#1083#1086#1074
    TabOrder = 1
    object LabelPathDir: TLabel
      Left = 9
      Top = 21
      Width = 25
      Height = 13
      Caption = #1055#1091#1090#1100
    end
    object LabelStatusCreate: TLabel
      Left = 12
      Top = 114
      Width = 292
      Height = 13
      Constraints.MinWidth = 292
      WordWrap = True
    end
    object LabelWidthAtlas: TLabel
      Left = 227
      Top = 56
      Width = 78
      Height = 13
      Alignment = taRightJustify
      Caption = #1064#1080#1088#1080#1085#1072' '#1072#1090#1083#1072#1089#1072
    end
    object LabelAtlasType: TLabel
      Left = 8
      Top = 55
      Width = 56
      Height = 13
      Alignment = taRightJustify
      Caption = #1058#1080#1087' '#1072#1090#1083#1072#1089#1072
      ParentShowHint = False
      ShowHint = True
    end
    object LabelHieghtAtlas: TLabel
      Left = 230
      Top = 84
      Width = 75
      Height = 13
      Alignment = taRightJustify
      Caption = #1042#1099#1089#1086#1090#1072' '#1072#1090#1083#1072#1089#1072
    end
    object BtnBrowseDir: TButton
      Left = 315
      Top = 17
      Width = 76
      Height = 23
      Caption = #1054#1073#1079#1086#1088'...'
      TabOrder = 0
      OnClick = BtnBrowseDirClick
    end
    object BtnCreateAtlas: TButton
      Left = 315
      Top = 110
      Width = 76
      Height = 24
      Caption = #1057#1086#1079#1076#1072#1090#1100
      TabOrder = 1
      OnClick = BtnCreateAtlasClick
    end
    object EditDirFiles: TEdit
      Left = 40
      Top = 18
      Width = 265
      Height = 21
      TabOrder = 2
    end
    object ComboBoxWidthAtlas: TComboBox
      Left = 316
      Top = 52
      Width = 74
      Height = 21
      Style = csDropDownList
      ItemIndex = 1
      TabOrder = 3
      Text = '4096'
      Items.Strings = (
        '8192'
        '4096'
        '2048'
        '1024'
        '512')
    end
    object ComboBoxAtlasType: TComboBox
      Left = 69
      Top = 52
      Width = 138
      Height = 21
      Hint = 
        'PNG WoT - '#1072#1090#1083#1072#1089' '#1074' '#1092#1086#1088#1084#1072#1090#1077' PNG '#1076#1083#1103' World of Tanks'#13#10'DDS WoT - '#1072#1090#1083#1072 +
        #1089' '#1074' '#1092#1086#1088#1084#1072#1090#1077' DDS '#1076#1083#1103' World of Tanks'#13#10'DDS WoW - '#1072#1090#1083#1072#1089' '#1074' '#1092#1086#1088#1084#1072#1090#1077' DD' +
        'S '#1076#1083#1103' World of Warships'
      Style = csDropDownList
      ItemIndex = 0
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      Text = 'PNG WoT'
      OnSelect = ComboBoxAtlasTypeSelect
      Items.Strings = (
        'PNG WoT'
        'DDS WoT'
        'DDS WoWs'
        'DDS WoWs '#1073#1077#1079' MIP')
    end
    object CheckBoxTrimAtlas: TCheckBox
      Left = 9
      Top = 83
      Width = 160
      Height = 17
      Alignment = taLeftJustify
      BiDiMode = bdRightToLeft
      Caption = #1054#1073#1088#1077#1079#1072#1090#1100' '#1089#1074#1086#1073#1086#1076#1085#1086#1077' '#1084#1077#1089#1090#1086
      ParentBiDiMode = False
      TabOrder = 5
    end
    object ComboBoxHieghtAtlas: TComboBox
      Left = 316
      Top = 81
      Width = 74
      Height = 21
      Style = csDropDownList
      ItemIndex = 5
      TabOrder = 6
      Text = #1040#1074#1090#1086
      Items.Strings = (
        '8192'
        '4096'
        '2048'
        '1024'
        '512'
        #1040#1074#1090#1086)
    end
  end
  object ComboBoxSelectLang: TComboBox
    Left = 335
    Top = 257
    Width = 75
    Height = 21
    Style = csDropDownList
    TabOrder = 2
    OnSelect = ComboBoxSelectLangSelect
  end
  object OpenPicDialogAtlas: TOpenPictureDialog
    Filter = 
      #1042#1089#1077' '#1090#1080#1087#1099' '#1080#1079#1086#1073#1088#1072#1078#1077#1085#1080#1081' (*.png, *.dds)|*.png; *.dds|Portable Networ' +
      'k Graphics (*.png)|*.png|DirectDraw Surface (*.dds)|*.dds|'#1042#1089#1077' (*' +
      '.*)|*.*'
    Left = 224
    Top = 56
  end
  object OpenDialog1: TOpenDialog
    Options = [ofHideReadOnly, ofCreatePrompt, ofEnableSizing]
    Left = 160
    Top = 255
  end
end
