unit CreationAtlas;

interface

uses Classes, Vcl.Imaging.pngimage, Winapi.Windows, SysUtils, Math, Vcl.Graphics, Constants, Generics.Collections,
  Generics.Defaults;

type
  TFilePNG = class
  private
    FX: Integer;
    FY: Integer;
    FHeight: Integer;
    FWidth: Integer;
    FrX: Integer;
    FbY: Integer;
    FRct: TRect;
    procedure SetX(Value: Integer);
    procedure SetY(Value: Integer);
    procedure SetHeight(Value: Integer);
    procedure SetWidth(Value: Integer);
  public
    FileName: string;
    Image: TPngImage;
    MaxSize: Integer;
    property Rct: TRect read FRct;
    property rX: Integer read FrX;
    property bY: Integer read FbY;
    property X: Integer read FX write SetX;
    property Y: Integer read FY write SetY;
    property Height: Integer read FHeight write SetHeight;
    property Width: Integer read FWidth write SetWidth;
    constructor Create;
    destructor Destroy; override;
  end;

function CreateAtlas(const Path: string; AtlasName: TFileName; Width, Height: Integer; const AtlasType: TAtlasType;
  isTrimAtlas: Boolean): TIdErorr;

implementation

uses CrystalDXT; // , stopwatch;

const
  SIZE_TBGRA = sizeof(TBGRA);

var
  ArrayImage, B: array of TFilePNG;
  Array4PBGA: array [0 .. 3] of Integer;
  CurrentRow: Integer;

function CompareRect(const Rect1, Rect2: TRect): Integer;
begin
  if Rect1.Bottom = Rect2.Bottom then
    Result := Rect1.Left - Rect2.Left
  else
    Result := Rect1.Bottom - Rect2.Bottom;
end;

// ������ ���������� ���-�����������
function Arrangement(const TextureWidth: Integer; var TextureHeight: Integer; const isTrimAtlas: Boolean): TIdErorr;
var
  Occupied: Tlist<TRect>;
  I, J, K, CurrentY, CurrentHeight: Integer;
  FPNG: TFilePNG;
  RectImg: TRect;
begin
  if TextureHeight = 0 then
    CurrentHeight := 4096
  else
    CurrentHeight := TextureHeight;
  Occupied := Tlist<TRect>.Create(TComparer<TRect>.Construct(CompareRect));
  Occupied.Capacity := Length(ArrayImage);
  try
    with ArrayImage[0] do
      Occupied.Add(Rect(X, Y, rX, bY));
    K := 0;
    for I := 1 to Length(ArrayImage) - 1 do
    begin
      FPNG := ArrayImage[I];
      if (FPNG.Width >= ArrayImage[I - 1].Width) and (FPNG.Height >= ArrayImage[I - 1].Height) then
        FPNG.Y := Occupied[K].Bottom + 1
      else
        K := 0;
      J := K;
      CurrentY := CurrentHeight;
      with FPNG do
      begin
        // ����� ���������� �����
        RectImg := Rect(X, Y, rX + 1, bY + 1);
        while J < Occupied.Count do
        begin
          if Occupied[J].IntersectsWith(RectImg) then
          begin
            X := Occupied[J].Right + 1;
            if (Occupied[J].Bottom > Y) and (CurrentY > Occupied[J].Bottom) then
              CurrentY := Occupied[J].Bottom;
            if (rX > TextureWidth) then
            begin
              Y := CurrentY + 1;
              X := 0;
              CurrentY := CurrentHeight;
              while (K < Occupied.Count - 1) and (Y > Occupied[K].Bottom) do
                Inc(K);
            end;
            J := K;
            RectImg := Rect(X, Y, rX + 1, bY + 1);
          end
          else
            Inc(J);
        end;

        if (bY > CurrentHeight) then
          if (TextureHeight <> 0) then
            Exit(erSmallSizeAtlas)
          else
            CurrentHeight := bY;
        Occupied.BinarySearch(Rct, J);
        Occupied.Insert(J, Rct);
      end;
    end;
    if isTrimAtlas or (TextureHeight = 0) then
      TextureHeight := Occupied.Last.Bottom + ((4 - (Occupied.Last.Bottom and 3)) and 3);
    Result := erNotEerror;
  finally
    FreeAndNil(Occupied);
  end;
end;

// callback to fill bgra block from 32-bit bitmap

procedure CreateAtlasPNG(const FileName: TFileName; const TextureWidth, TextureHeight: Integer);
var
  AtlasImg: TPngImage;
  I, J: Integer;
  PAScanlineAtlas, PAScanlineImage, BArray: PByte;
begin
  AtlasImg := TPngImage.CreateBlank(COLOR_RGBALPHA, 8, TextureWidth, TextureHeight);
  AtlasImg.CompressionLevel := 2;
  GetMem(BArray, TextureWidth + 1);
  for I := 0 to TextureWidth do
    (BArray + I)^ := 255;
  try
    for I := 0 to Length(ArrayImage) - 1 do
      with ArrayImage[I] do
      begin
        BitBlt(AtlasImg.Canvas.Handle, X, Y, Width, Height, Image.Canvas.Handle, 0, 0, SRCCOPY);
        for J := Y to bY - 1 do
        begin
          PAScanlineAtlas := PByte(AtlasImg.AlphaScanline[J]);
          PAScanlineImage := PByte(Image.AlphaScanline[J - Y]);
          Inc(PAScanlineAtlas, X);
          if (PAScanlineImage <> nil) then
            CopyMemory(PAScanlineAtlas, PAScanlineImage, Width)
          else
            CopyMemory(PAScanlineAtlas, BArray, Width);
        end;
      end;
    AtlasImg.SaveToFile(FileName);
  finally
    FreeMem(BArray);
    FreeAndNil(AtlasImg);
  end;
end;

procedure GetBitmap32bitBlock(const Handle: pointer; var BGRABlock: TBGRABlock; const X, Y: Integer);
var
  Bitmap: TBitmap;
  Src, Dest: PBGRA;
  I, J, Width, Height, s_TBGRA: Integer;
begin
  Bitmap := TBitmap(Handle);
  Width := Bitmap.Width;
  Height := Bitmap.Height;

  Src := nil;
  Dest := @BGRABlock[0];
  s_TBGRA := SIZE_TBGRA * X;
  if (Y + 3 < Height) and (X + 3 < Width) then
  begin
    if CurrentRow <> Y then
    begin
      for I := 0 to 3 do
        Array4PBGA[I] := Integer(Bitmap.ScanLine[Y + I]);
      CurrentRow := Y;
    end;
    for J := 0 to 3 do
    begin
      Src := pointer(Array4PBGA[J] + s_TBGRA);
      for I := 0 to 3 do
      begin
        Dest^ := Src^;
        Inc(Dest);
        Inc(Src);
      end;
    end;
  end
  else
    for J := Y to Y + 3 do
    begin
      if (J < Height) then
        Src := pointer(Integer(Bitmap.ScanLine[J]) + s_TBGRA);
      for I := X to X + 3 do
      begin
        if (I >= Width) or (J >= Height) then
          Dest^.A := 0
        else
          Dest^ := Src^;
        Inc(Dest);
        Inc(Src);
      end;
    end;

end;

procedure SaveDDSImage(const Image32bit: TBitmap; const FileName: TFileName; const DxtFormat: AtfFormat; const isMinMaps: Boolean);
var
  Bitmap: TBitmap;
  Header: TDxtHeader;
  Buffer: TMemoryStream;
  DxtBlocks: pointer;
  Size : UInt64;
  Sizes: array of Integer;
  NumMips, I, Width, Height: Integer;
begin
  Bitmap := TBitmap.Create;
  try
    Bitmap.Assign(Image32bit);
    if isMinMaps then
    begin
      Width := Bitmap.Width;
      Height := Bitmap.Height;
      NumMips:= Trunc(log2(Max(Width, Height))) + 1;
      SetLength(Sizes, NumMips);
      Size := 0;
      for I := 0 to NumMips - 1 do
      begin
        Sizes[I] := DxtImageSize(DxtFormat, Width, Height);
        Inc(Size, Sizes[I]);
        if Width > 1 then Width := Width div 2;
        if Height > 1 then Height := Height div 2;
      end;
    end
    else
    begin
      NumMips := 1;
      Size := DxtImageSize(DxtFormat, Bitmap.Width, Bitmap.Height);
    end;

    Header := DxtHeader(Bitmap.Width, Bitmap.Height, NumMips, DxtFormat);
    Buffer := TMemoryStream.Create;
    try
      Buffer.Write(Header, SizeOf(Header));
      Buffer.Size := SizeOf(Header) + Size;
      Size:=Integer(Buffer.Memory)+SizeOf(Header);
      DxtBlocks:=Pointer(Size);
      CurrentRow := -1;
      if isMinMaps then
        for I := 0 to NumMips - 1 do
        begin
          DxtImageCompressParallel(DxtBlocks, (DxtFormat <> DXT5), Bitmap.Width, Bitmap.Height, pointer(Bitmap),
            GetBitmap32bitBlock);
          Inc(Size, Sizes[I]);
          DxtBlocks:=Pointer(Size);
          MipMapCompress(Bitmap);
        end
      else
        DxtImageCompressParallel(DxtBlocks, (DxtFormat <> DXT5), Header.dwWidth, Header.dwHeight, pointer(Bitmap),
          GetBitmap32bitBlock);
      Buffer.SaveToFile(FileName);
    finally
      Buffer.Free;
    end;
  finally
    FreeAndNil(Bitmap);
    SetLength(Sizes, 0);
  end;
end;

procedure CreateAtlasDXT5(const FileName: TFileName; const TextureWidth, TextureHeight: Integer; const isMinMaps: Boolean);
var
  AtlasImg: TBitmap;
  I, J: Integer;
  ByteInRow, K: Cardinal;
  A: Byte;
  PScanlineAtlas, PAScanlineImage, BArray: PByte;
begin
  A := 255;
  AtlasImg := TBitmap.Create;
  ByteInRow := TextureWidth shl 2;
  BArray := AllocMem(ByteInRow);
  try
    AtlasImg.PixelFormat := pf32bit;
    AtlasImg.Width := TextureWidth;
    AtlasImg.Height := TextureHeight;
    for I := 0 to TextureHeight - 1 do
      CopyMemory(PByte(DWord(AtlasImg.ScanLine[I])), BArray, ByteInRow);
    for I := 0 to Length(ArrayImage) - 1 do
      with ArrayImage[I] do
      begin
        BitBlt(AtlasImg.Canvas.Handle, X, Y, Width, Height, Image.Canvas.Handle, 0, 0, SRCCOPY);
        for J := Y to bY - 1 do
        begin
          PScanlineAtlas := AtlasImg.ScanLine[J];
          Inc(PScanlineAtlas, X shl 2 + 3);
          PAScanlineImage := PByte(Image.AlphaScanline[J - Y]);
          if PAScanlineImage <> nil then
            for K := 0 to Width - 1 do
            begin
              CopyMemory(PScanlineAtlas, PAScanlineImage, 1);
              Inc(PScanlineAtlas, 4);
              Inc(PAScanlineImage, 1);
            end
          else
            for K := 0 to Width - 1 do
            begin
              CopyMemory(PScanlineAtlas, @A, 1);
              Inc(PScanlineAtlas, 4);
            end;
        end;
      end;
    SaveDDSImage(AtlasImg, FileName, DXT5, isMinMaps);
  finally
    FreeMem(BArray);
    FreeAndNil(AtlasImg);
  end;
end;

function CreateAtlasXML(const FileName: TFileName): Integer;
var
  I: Integer;
  EncodingUTF8: TEncoding;
begin
  EncodingUTF8 := TEncoding.UTF8;
  with TStringList.Create do
    try
      BeginUpdate;
      Append('<root>');
      for I := 0 to Length(ArrayImage) - 1 do
      begin
        Append('  <SubTexture>');
        Append('    <name> ' + ChangeFileExt(ArrayImage[I].FileName, '') + ' </name>');
        Append('    <x> ' + IntToStr(ArrayImage[I].X) + ' </x>');
        Append('    <y> ' + IntToStr(ArrayImage[I].Y) + ' </y>');
        Append('    <width> ' + IntToStr(ArrayImage[I].Width) + ' </width>');
        Append('    <height> ' + IntToStr(ArrayImage[I].Height) + ' </height>');
        Append('  </SubTexture>');
      end;
      Append('</root>');
      EndUpdate;
      SaveToFile(FileName, EncodingUTF8);
    finally
      Free;
    end;
  Result := 1;
end;

function CreateAtlasAtlas(const AFileName: TFileName; const AWidth, AHeight: Integer): Integer;
var
  I: Integer;
  AFormatSettings: TFormatSettings;
begin
  AFormatSettings := TFormatSettings.Create;
  AFormatSettings.DecimalSeparator := '.';
  AFormatSettings.CurrencyDecimals := 6;
  with TStringList.Create do
    try
      BeginUpdate;
      Append('page "' + ChangeFileExt(ExtractFileName(AFileName), '.dds" ') + IntToStr(AWidth) + ' ' + IntToStr(AHeight));
      Append('{');
      for I := 0 to Length(ArrayImage) - 1 do
        with ArrayImage[I] do
          Append(chr(VK_tab) + Format('"%s" { %0.6f %0.6f %0.6f %0.6f }',
                 [ChangeFileExt(FileName, ''), X / AWidth, Y / AHeight, (X + Width) / AWidth, (Y + Height) / AHeight],
                 AFormatSettings));
      Append('}');
      EndUpdate;
      SaveToFile(AFileName);
    finally
      Free;
    end;
  Result := 1;
end;

function InitArrayImg(const Path: string): TIdErorr;
var
  SR: TSearchRec;
  I: Integer;
begin
  if FindFirst(Path + '\*.png', faAnyFile, SR) = 0 then
  begin
    I := 0;
    SetLength(ArrayImage, 1000);
    repeat
      if (SR.Attr <> faDirectory) then
      begin
        ArrayImage[I] := TFilePNG.Create;
        with ArrayImage[I] do
        begin
          FileName := SR.Name;
          Image.LoadFromFile(Path + '\' + FileName);
          Height := Image.Height;
          Width := Image.Width;
          MaxSize := Max(Width, Height);
        end;
        Inc(I);
        if I = Length(ArrayImage) then
          SetLength(ArrayImage, Length(ArrayImage) + 500);

      end;
    until FindNext(SR) <> 0;
    FindClose(SR);
    SetLength(ArrayImage, I);
    Result := erNotEerror;
  end
  else
    Result := erImagesPngNotFound;
end;

function RadixSort: Integer;
var
  I, J, t, CInd: Integer;
  C: array [0 .. 9] of Integer;
begin
  t := 1;
  SetLength(B, Length(ArrayImage));
  for I := 1 to 4 do
  begin
    For J := 0 to 9 do
      C[J] := 0;
    For J := 0 to Length(ArrayImage) - 1 do
    begin
      CInd := (ArrayImage[J].MaxSize mod (t * 10)) div t;
      C[CInd] := C[CInd] + 1;
    end;
    For J := 8 downto 0 do
      C[J] := C[J + 1] + C[J];
    For J := Length(ArrayImage) - 1 downto 0 do
    begin
      CInd := (ArrayImage[J].MaxSize mod (t * 10)) div t;
      B[C[CInd] - 1] := ArrayImage[J];
      C[CInd] := C[CInd] - 1;
    end;
    t := t * 10;
    ArrayImage := Copy(B);
  end;
  SetLength(B, 0);
  Result := 1;
end;

function CreateAtlas(const Path: string; AtlasName: TFileName; Width, Height: Integer; const AtlasType: TAtlasType;
  isTrimAtlas: Boolean): TIdErorr;
var
  I: Integer;
begin
  try
    Result := InitArrayImg(Path);
    if AtlasName = '' then
      AtlasName := IncludeTrailingPathDelimiter(Path) + 'Atlas\' + ExtractFileName(ExcludeTrailingPathDelimiter(Path)
        ) + '.png';
    if Result = erNotEerror then
    begin
      RadixSort;
      Result := Arrangement(Width, Height, isTrimAtlas);
      if Result = erNotEerror then
      begin
        ForceDirectories(ExtractFilePath(AtlasName));
        case AtlasType of
          atPNG:
          begin
            CreateAtlasPNG(ChangeFileExt(AtlasName, '.png'), Width, Height);
            CreateAtlasXML(ChangeFileExt(AtlasName, '.xml'));
          end;
          atDXT5:
          begin
            CreateAtlasDXT5(ChangeFileExt(AtlasName, '.dds'), Width, Height, False);
            CreateAtlasXML(ChangeFileExt(AtlasName, '.xml'));
          end;
          atDXT5w:
          begin
            CreateAtlasDXT5(ChangeFileExt(AtlasName, '.dds'), Width, Height, True);
            CreateAtlasAtlas(ChangeFileExt(AtlasName, '.atlas'), Width, Height);
          end;
          atDXT5w_withoutMIP:
          begin
            CreateAtlasDXT5(ChangeFileExt(AtlasName, '.dds'), Width, Height, False);
            CreateAtlasAtlas(ChangeFileExt(AtlasName, '.atlas'), Width, Height);
          end;
        end;
      end;
    end;
  finally
    for I := 0 to Length(ArrayImage) - 1 do
      FreeAndNil(ArrayImage[I]);
    SetLength(ArrayImage, 0);
  end;
end;

{ TFilePNG }

constructor TFilePNG.Create;
begin
  inherited;
  Image := TPngImage.Create;
  X := 0;
  Y := 0;
  Height := 0;
  Width := 0;
end;

destructor TFilePNG.Destroy;
begin
  FreeAndNil(Image);
  FileName := '';
  inherited;
end;

procedure TFilePNG.SetHeight(Value: Integer);
begin
  FHeight := Value;
  FbY := FY + FHeight;
  FRct := Rect(FX, FY, FrX, FbY);
end;

procedure TFilePNG.SetWidth(Value: Integer);
begin
  FWidth := Value;
  FrX := FX + FWidth;
  FRct := Rect(FX, FY, FrX, FbY);
end;

procedure TFilePNG.SetX(Value: Integer);
begin
  FX := Value;
  FrX := FX + FWidth;
  FRct := Rect(FX, FY, FrX, FbY);
end;

procedure TFilePNG.SetY(Value: Integer);
begin
  FY := Value;
  FbY := FY + FHeight;
  FRct := Rect(FX, FY, FrX, FbY);
end;

end.
