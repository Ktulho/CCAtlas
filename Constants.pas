unit Constants;

interface

type
  TIdErorr = (erNotEerror, erUnsupportedFormat, erFilesNotFound, erXmlNotFound, erImageNotFound, erPathNotSpecified,
    erImagesPngNotFound, erAtlasNotCreate, erSmallSizeAtlas, erHeightIncorrectly, erWidthIncorrectly,
    erAtlasTypeIncorrectly, erFolderNotFound, erNotFoundKey, erNotWotOrDist, erNotFindZip, erMissingKey, erKeyNotExist);
  TAtlasType = (atPNG, atDXT5, atDXT5w, atDXT5w_withoutMIP);

const
  CUT_ATLAS = 'ca';              // Cut the Atlas into separate images.
  ASSEMBLE_ATLAS = 'aa';         // Assemble an Atlas of images.
  CUT_BATTLE_ATLAS = 'cba';      // Extract and split the battleAtlas into separate images.
  CUT_MARKER_ATLAS = 'cvma';     // Extract and split the vehicleMarkerAtlas into separate images.
  CUT_DAMAGE_INDICATOR = 'cdia'; // Extract and split the damageIndicator into separate images.
  CUT_BATTLE_LOBBY = 'cbla';     // Extract and split the commonBattleLobby into separate images.
  CUT_COMPONENTS_ATLAS = 'cca';  // Extract and split the components into separate images.
  CUT_MAPS_BLACKLIST = 'cmba';   // Extract and split the mapsBlacklist into separate images.
  CUT_QUESTS_PROGRESS = 'cqpa';  // Extract and split the questsProgress into separate images.
  CUT_STORE_ATLAS = 'csa';       // Extract and split the store into separate images.
  WIDTH_ATLAS = 'w';
  HEIGHT_ATLAS = 'h';
  ATLAS_TYPE = 'a';
  TRIM_ATLAS = 't';
  ID_CUT_ATLAS = 0;
  ID_ASSEMBLE_ATLAS = 1;
  ID_CUT_BATTLE_ATLAS = 2;
  ID_CUT_MARKER_ATLAS = 3;
  ID_CUT_DAMAGE_INDICATOR = 4;
  ID_CUT_BATTLE_LOBBY = 5;
  ID_CUT_COMPONENTS_ATLAS = 6;
  ID_CUT_MAPS_BLACKLIST = 7;
  ID_CUT_QUESTS_PROGRESS = 8;
  ID_CUT_STORE_ATLAS = 9;
  ID_WIDTH_ATLAS = 10;
  ID_HEIGHT_ATLAS = 11;
  ID_ATLAS_TYPE = 12;
  ID_TRIM_ATLAS = 13;
  NUMBER_PARAMETERS = 14;
  ALL_PARAMETERS: array [0 .. NUMBER_PARAMETERS - 1] of string = (CUT_ATLAS, ASSEMBLE_ATLAS, CUT_BATTLE_ATLAS,
    CUT_MARKER_ATLAS, CUT_DAMAGE_INDICATOR, CUT_BATTLE_LOBBY, CUT_COMPONENTS_ATLAS, CUT_MAPS_BLACKLIST,
    CUT_QUESTS_PROGRESS, CUT_STORE_ATLAS, WIDTH_ATLAS, HEIGHT_ATLAS, ATLAS_TYPE, TRIM_ATLAS);

  NAMES_PKG: array [0..2] of string = ('gui-part1', 'gui-part2', 'gui');
  ATLAS_EXTENSION: array [0..3] of string = ('.png', '.dds', '.dds', '.dds');

implementation

end.
