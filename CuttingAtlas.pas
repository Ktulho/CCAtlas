unit CuttingAtlas;

interface

uses Classes, SysUtils, Vcl.Dialogs, XMLIntf, XMLDoc, Winapi.Windows, Vcl.Imaging.pngimage, Vcl.Graphics, Constants,
  IOUtils;

function CutAtlas(const FileNameImg: TFileName; DistFolder: string = ''): TIdErorr;

implementation

uses language, CrystalDXT;

const
  SIZE_TBGRA = sizeof(TBGRA);

type
  TSubTexture = record
    FName: string;
    LocRect: TRect;
  end;

var
  RootXML: array of TSubTexture;
  Array4PBGA: array [0 .. 3] of Integer;
  CurrentRow: Integer;

  // callback to fill image block from bgra block
procedure SetBitmap32bitBlock(const Handle: pointer; var BGRABlock: TBGRABlock; const X, Y: Integer);
var
  Bitmap: TBitmap;
  Src, Dest: PBGRA;
  i, j, Width, Height, s_TBGRA: Integer;
begin
  Bitmap := TBitmap(Handle);
  Width := Bitmap.Width;
  Height := Bitmap.Height;
  Dest := nil;
  Src := @BGRABlock[0];
  s_TBGRA := SIZE_TBGRA * X;
  if (Y + 3 < Height) and (X + 3 < Width) then
  begin
    if CurrentRow <> Y then
    begin
      for i := 0 to 3 do
        Array4PBGA[i] := Integer(Bitmap.ScanLine[Y + i]);
      CurrentRow := Y;
    end;
    for j := 0 to 3 do
    begin
      Dest := pointer(Array4PBGA[j] + s_TBGRA);
      CopyMemory(Dest, Src, 4 * SIZE_TBGRA);
      Inc(Src, 4);
    end;
  end
  else
    for j := Y to Y + 3 do
    begin
      if (j < Height) then
        Dest := pointer(Integer(Bitmap.ScanLine[j]) + SIZE_TBGRA);
      for i := X to X + 3 do
      begin
        if (i < Width) and (j < Height) then
          Dest^ := Src^;
        Inc(Dest);
        Inc(Src);
      end;
    end;
end;

// load dds (DXT1 and DXT5) image
function LoadDDSImage(const FileName: TFileName; var DefaultAlpha: TDxtAlphaKind): TBitmap;
var
  DXT1: boolean;
  Header: TDxtHeader;
  Buffer: TMemoryStream;
  DxtBlocks: pointer;
begin
  Result := nil;
  Buffer := TMemoryStream.Create;
  try
    Buffer.LoadFromFile(FileName);
    if (Buffer.Read(Header, sizeof(Header)) <> sizeof(Header)) then
      exit;
    if (Header.dwFourCC <> DDS_DXT1) and (Header.dwFourCC <> DDS_DXT5) then
    begin
      ShowMessage(IniLanguage.InvalidImage);
      exit; // not supported
    end;
    DXT1 := (Header.dwFourCC = DDS_DXT1);
    DxtBlocks := pointer(Integer(Buffer.Memory) + sizeof(Header));
    DefaultAlpha := DxtImageAlpha(DxtBlocks, DXT1, Header.dwWidth, Header.dwHeight);

    // Decompress
    try
      Result := TBitmap.Create;
      Result.PixelFormat := pf32bit;
      Result.Width := Header.dwWidth;
      Result.Height := Header.dwHeight;
      CurrentRow := -1;
      DxtImageDecompress(DxtBlocks, DXT1, Header.dwWidth, Header.dwHeight, pointer(Result), SetBitmap32bitBlock);
    except
      FreeAndNil(Result);
    end;
  finally
    FreeAndNil(Buffer);
  end;
end;

procedure CutDDS(const FileName: TFileName; const DistFolder: string);
var
  AtlasImg: TBitmap;
  Image: TPngImage;
  i, j: Integer;
  K, LengthRootXML: Cardinal;
  PScanlineAtlas, PScanlineImage, PAScanlineImage: PByte;
  DefaultAlpha: TDxtAlphaKind;
begin
  ForceDirectories(DistFolder);
  AtlasImg := LoadDDSImage(FileName, DefaultAlpha);
  try
    AtlasImg.PixelFormat := pf32bit;
    LengthRootXML := Length(RootXML) - 1;
    for i := 0 to LengthRootXML do
      with RootXML[i].LocRect do
        try
          Image := TPngImage.CreateBlank(COLOR_RGBALPHA, 8, Width, Height);
          Image.CompressionLevel := 2;
          for j := 0 to Height - 1 do
          begin
            PScanlineAtlas := AtlasImg.ScanLine[j + Top];
            Inc(PScanlineAtlas, Left shl 2);
            PScanlineImage := Image.ScanLine[j];
            PAScanlineImage := PByte(Image.AlphaScanline[j]);
            for K := 0 to Width - 1 do
            begin
              CopyMemory(PScanlineImage, PScanlineAtlas, 3);
              Inc(PScanlineAtlas, 3);
              Inc(PScanlineImage, 3);
              PAScanlineImage^ := PScanlineAtlas^;
              Inc(PScanlineAtlas);
              Inc(PAScanlineImage);
            end;
          end;
          Image.SaveToFile(DistFolder + RootXML[i].FName + '.png');
        finally
          FreeAndNil(Image);
        end;
  finally
    FreeAndNil(AtlasImg);
    SetLength(RootXML, 0);
  end;
end;

procedure CutPNG(const FileName: TFileName; const DistFolder: string);
var
  Image, AtlasImg: TPngImage;
  i, j: Integer;
  PAScanlineAtlas: PByte;
begin
  ForceDirectories(DistFolder);
  AtlasImg := TPngImage.Create;
  try
    AtlasImg.LoadFromFile(FileName);
    for i := 0 to Length(RootXML) - 1 do
      with RootXML[i].LocRect do
        try
          Image := TPngImage.CreateBlank(COLOR_RGBALPHA, 8, Width, Height);
          Image.CompressionLevel := 2;
          BitBlt(Image.Canvas.Handle, 0, 0, Width, Height, AtlasImg.Canvas.Handle, Left, Top, SRCCOPY);
          for j := 0 to Height - 1 do
          begin
            PAScanlineAtlas := PByte(AtlasImg.AlphaScanline[j + Top]);
            Inc(PAScanlineAtlas, Left);
            if PAScanlineAtlas <> nil then
              CopyMemory(Image.AlphaScanline[j], PAScanlineAtlas, Width)
            else
              FillMemory(Image.AlphaScanline[j], AtlasImg.Width, $FF);
          end;
          Image.SaveToFile(DistFolder + RootXML[i].FName + '.png');
        finally
          FreeAndNil(Image);
        end;
  finally
    FreeAndNil(AtlasImg);
    SetLength(RootXML, 0);
  end;

end;

procedure ParsingXML(const FileNameXML: TFileName);
var
  i, j: Integer;
  AtlasXML: IXMLDocument;
begin
  AtlasXML := TXMLDocument.Create(nil);
  AtlasXML.FileName := FileNameXML;
  AtlasXML.Active := True;
  SetLength(RootXML, AtlasXML.DocumentElement.ChildNodes.Count);
  j := 0;
  for i := 0 to AtlasXML.DocumentElement.ChildNodes.Count - 1 do
    if AtlasXML.DocumentElement.ChildNodes[i].NodeType = TNodeType.ntElement then
      with RootXML[j], AtlasXML.DocumentElement.ChildNodes[i] do
      begin
        FName := Trim(ChildNodes['name'].Text);
        LocRect.Left := StrToInt(Trim(ChildNodes['x'].Text));
        LocRect.Top := StrToInt(Trim(ChildNodes['y'].Text));
        LocRect.Right := StrToInt(Trim(ChildNodes['width'].Text)) + LocRect.Left;
        LocRect.Bottom := StrToInt(Trim(ChildNodes['height'].Text)) + LocRect.Top;
        Inc(j);
      end;
  SetLength(RootXML, j);
  AtlasXML.Active := False;
end;

procedure ParsingAtlas(const FileNameAtlas: TFileName);
var
  ImageParameters, Atlas: TStringList;
  I, J, Width, Height, IndexImage: Integer;
  AFormatSettings: TFormatSettings;
begin
  Atlas := TStringList.Create();
  ImageParameters := TStringList.Create();
  Atlas.LoadFromFile(FileNameAtlas);
  ImageParameters.Delimiter := ' ';
  ImageParameters.DelimitedText := Atlas[0];
  Width := StrToInt(ImageParameters[2]);
  Height := StrToInt(ImageParameters[3]);
  SetLength(RootXML, Atlas.Count - 1);
  IndexImage := 0;
  AFormatSettings := TFormatSettings.Create;
  AFormatSettings.DecimalSeparator := '.';
  for I := 1 to Atlas.Count - 1 do
    with RootXML[IndexImage] do
    begin
      ImageParameters.DelimitedText := Trim(Atlas[I]);
      if ImageParameters.Count < 5 then
        Continue;
      J := 0;
      ImageParameters.BeginUpdate;
      while J < ImageParameters.Count do
        if (ImageParameters[J] = '{') or  (ImageParameters[J] = '}') then
          ImageParameters.Delete(J)
        else
          Inc(J);
      ImageParameters.EndUpdate;
      FName := Trim(ImageParameters[0]);
      LocRect.Left := Round(StrToFloat(ImageParameters[1], AFormatSettings) * Width);
      LocRect.Top := Round(StrToFloat(ImageParameters[2], AFormatSettings) * Height);
      LocRect.Right := Round(StrToFloat(ImageParameters[3], AFormatSettings) * Width);
      LocRect.Bottom := Round(StrToFloat(ImageParameters[4], AFormatSettings) * Height);
      Inc(IndexImage);
    end;
  SetLength(RootXML, IndexImage);
  FreeAndNil(ImageParameters);
  FreeAndNil(Atlas);
end;

function CutAtlas(const FileNameImg: TFileName; DistFolder: string = ''): TIdErorr;
const
  PNG = UInt32($474E5089);
  DDS = UInt32($20534444);
var
  Magic: UInt32;
  FileName: string;
  Stream: TFileStream;
  AFile: TSearchRec;
begin

  FileName := ChangeFileExt(FileNameImg, '.xml');
  if FindFirst(FileName, faAnyFile, AFile) <> 0 then
    FileName := ChangeFileExt(FileNameImg, '.atlas');
  Result := erNotEerror;
  case Byte(FileExists(FileNameImg)) + (Byte(FileExists(FileName)) * 2) of
    0:
      Result := erFilesNotFound; // The files are not found.
    1:
      Result := erXmlNotFound; // File FileNameXML not found.
    2:
      Result := erImageNotFound; // File FileNameImg not found.
  end;
  if Result = erNotEerror then
  begin
    if ExtractFileExt(FileName) = '.xml' then
      ParsingXML(FileName)
    else
      ParsingAtlas(FileName);
    Magic := 0;
    Stream := TFileStream.Create(FileNameImg, fmOpenRead);
    try
      Stream.ReadBufferData(Magic);
    finally
      FreeAndNil(Stream);
    end;
    if DistFolder = '' then
      DistFolder := ExtractFilePath(FileNameImg) + TPath.GetFileNameWithoutExtension(FileNameImg) + '\';
    case Magic of
      PNG:
        CutPNG(FileNameImg, DistFolder);
      DDS:
        CutDDS(FileNameImg, DistFolder);
    else
      Result := erUnsupportedFormat;
    end;
  end;
end;

end.
