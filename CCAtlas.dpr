program CCAtlas;

uses
  Vcl.Forms, Winapi.Windows, Main in 'Main.pas' {FormMain} , CreationAtlas in 'CreationAtlas.pas',
  CuttingAtlas in 'CuttingAtlas.pas', language in 'language.pas', CrystalDXT in 'CrystalDXT.pas',
  MainConsole in 'MainConsole.pas', Constants in 'Constants.pas';

function AttachConsole(dwProcessID: UInt): bool; stdcall; external 'kernel32.dll';
function AllocConsole(): bool; stdcall; external 'kernel32.dll';
function FreeConsole(): bool; stdcall; external 'kernel32.dll';

{$R *.res}

procedure StartGUI();
begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFormMain, FormMain);
  Application.Run;
end;

begin
  if ParamCount > 0 then
  begin
    if AttachConsole(High(UInt)) then
    begin
      RunConsole();
      FreeConsole();
    end
    else if AllocConsole() then
      RunConsole()
    else
      StartGUI();
  end
  else
    StartGUI();

end.
